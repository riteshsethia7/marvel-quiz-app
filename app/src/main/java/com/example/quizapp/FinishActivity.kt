package com.example.quizapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_finish.*

class FinishActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish)
        supportActionBar?.hide()
        nameDisplay.text = "Hooray, ${intent.getStringExtra("userName")}"
        answeredCount.text = "You answered ${intent.getStringExtra("correctCount")} out of 10 questions"
    }

    fun playAgain(view : View){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}