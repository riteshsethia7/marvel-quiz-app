package com.example.quizapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var userName:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        supportActionBar?.hide()
    }

    fun onStartButton(view : View) {
        val name :String = nameInput.text.toString()
        if (name.isEmpty()) Toast.makeText(this,"Please Enter a name",Toast.LENGTH_SHORT).show()
        else{
            userName = nameInput.text.toString()
            val intent = Intent(this,QuizQuestionActivity::class.java)
            intent.putExtra("userName",userName)
            startActivity(intent)
            finish()
        }
    }
}