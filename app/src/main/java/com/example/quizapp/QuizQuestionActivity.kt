package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_quiz_question.*


class QuizQuestionActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var userName: String
    private lateinit var questions: ArrayList<Questions>
    private var questionNumber = 0
    private var correctAnswers = 0
    private var selectedOption = 0
    private var isSelectedOption = false
    private lateinit var options: List<TextView>
    private lateinit var currentQuestion :Questions
    private var isSubmitClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_question)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        supportActionBar?.hide()
        userName = intent.getStringExtra("userName").toString()
        options = listOf(option1, option2, option3, option4)
        readQuestionsJson()
        option1.setOnClickListener(this)
        option2.setOnClickListener(this)
        option3.setOnClickListener(this)
        option4.setOnClickListener(this)
    }

    private fun readQuestionsJson() {
        val json = resources.openRawResource(R.raw.questions).bufferedReader().use { it.readText() }
        val sType = object : TypeToken<ArrayList<Questions>>() {}.type
        questions = Gson().fromJson(json, sType)
        displayQuestion()
    }

    fun nextQuestion(view: View) {
        if (questionNumber < 10 && (view as Button).text == "NEXT" ) {
            isSubmitClicked = false
            selectedOption = 0
            displayQuestion()
        } else if (questionNumber < 10 && (view as Button).text == "SUBMIT"){
            if (isSelectedOption) {
                isSubmitClicked = true
                verifyOptionClicked()
                nextQuestion.text = "NEXT"
            }
            else {
                Toast.makeText(this, "Please Select a option", Toast.LENGTH_SHORT).show()
            }

        } else onSubmit()
    }

    private fun displayQuestion() {
        isSelectedOption = false
        currentQuestion = questions[questionNumber]
        defaultOptionsStyle()
        questionName.text = currentQuestion.question
        questionImage.setImageResource(resources.getIdentifier(currentQuestion.image, "drawable", packageName))
        option1.text = currentQuestion.option1
        option2.text = currentQuestion.option2
        option3.text = currentQuestion.option3
        option4.text = currentQuestion.option4
        progressBar.progress = questionNumber + 1
        progressText.text = "${questionNumber + 1}/10"
        nextQuestion.text = "SUBMIT"
    }

    private fun defaultOptionsStyle() {
        for (i in options) {
            i.setTextColor(ContextCompat.getColor(this, R.color.darkgray))
            i.background = ContextCompat.getDrawable(this, R.drawable.default_option_border)
        }
    }

    override fun onClick(v: View?) {
        if (!isSubmitClicked) {
            when (v?.id) {
                R.id.option1 -> selectedOptionStyle(1, option1)
                R.id.option2 -> selectedOptionStyle(2, option2)
                R.id.option3 -> selectedOptionStyle(3, option3)
                R.id.option4 -> selectedOptionStyle(4, option4)
            }
            isSelectedOption = true
        }

    }

    private fun selectedOptionStyle(selectedOption: Int, option: TextView) {
        defaultOptionsStyle()
        option.setTextColor(ContextCompat.getColor(this, R.color.black))
        option.background = ContextCompat.getDrawable(this, R.drawable.selected_option_border)
        this.selectedOption = selectedOption
    }

    private fun verifyOptionClicked() {
        defaultOptionsStyle()
        if (currentQuestion.correctOption == selectedOption) {
            correctOptionStyle(options[selectedOption - 1])
            correctAnswers++
        } else {
            correctOptionStyle(options[currentQuestion.correctOption - 1])
            wrongOptionStyle(options[selectedOption - 1])
        }
        questionNumber++
    }


    private fun correctOptionStyle(view: TextView){
        view.setTextColor(ContextCompat.getColor(this, R.color.green))
    }

    private fun wrongOptionStyle(view: TextView){
        view.setTextColor(ContextCompat.getColor(this, R.color.red))
    }


    private fun onSubmit() {
        val intent = Intent(this, FinishActivity::class.java)
        intent.putExtra("userName", userName)
        intent.putExtra("correctCount", correctAnswers.toString())
        startActivity(intent)
        finish()
    }
}