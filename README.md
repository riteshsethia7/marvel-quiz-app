## About the App

A simple quiz app which contains 10 questions with 4 options and one image each.
In the end it displays the number of correct options.

### How to edit the questions
- Look for questions.json (you will find it under raw in res)
- Edit the questions and options
- add your images in the drawable folder and make sure your images name are same as in this blueprint.
